import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/Colors';

export default function NotificationsScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Notification</Text>
    </View>
  );
}

NotificationsScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.tabsBackgroundColor,
    justifyContent: "center",
    alignItems: "center"
  },
  title:{
    color:Colors.lightColor,
    fontSize:20
  }
});
