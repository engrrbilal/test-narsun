import React, { Component } from 'react';
import {
  Container, Text, Button, Body, Card, CardItem,
  Header, Left, Right, Icon, Title, Input, Tabs, Tab, Segment
} from 'native-base';
import {StyleSheet, View } from 'react-native';
import LinksTabs from '../components/LinksTabs/LinksTabs';
import Colors from '../constants/Colors';
export default class LinksScreen extends Component {

  state = {
    tabPage: 1,
    searchTerm:null
  }
  componentDidMount() {
    console.log("componentDidMount LinksScreen")
    this.willFocusSubscription = this.props.navigation.addListener(
      "focus",
      () => {
        console.log("componentDidMount re-render for fetching new data")
      }
    );
  }
  searchForm(term) {
    this.setState({ searchTerm: term })
  }
  render() {
    return (
      <Container style={styles.container} >

        <Header>
          <Left style={{ flex:1}}>
            <Button transparent onPress={() => console.log("menu pressed ! ")}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body style={{ flex:1}}>
            <Title>Results</Title>
          </Body>

          <Right style={{ flex:1}}>
            <Button transparent>
              <Icon name='filter' type="AntDesign"/>
            </Button>
          </Right>
        </Header>
        <View style={styles.bodyContainer}>
          <View style={styles.borderBoxStyle}>
            <Input placeholder="Search for a person or rating" onChangeText={(term) => this.searchForm(term)} />
            <Icon name="ios-search" style={{ fontSize: 28, color: '#555555' }} />
          </View>
          <LinksTabs searchTerm={this.state.searchTerm} navigation={this.props.navigation}/>
        </View>
      </Container >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    textAlign: "center"
  },
  bodyContainer: {
    backgroundColor: Colors.tabsBackgroundColor,
    height: "100%"
  },
  borderBoxStyle: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#d6d7da",
    backgroundColor: "#ffff",
    paddingRight: 10,
    paddingLeft: 10,
    height: 50,
    alignSelf: "center",
    width: "98%",
    marginTop: 5,
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
});