const tintColor = '#2f95dc';

export default {
  tabsBackgroundColor:"#1e1e1e",
  tabsThemeColor:"#19502c",
  tabsSepratorColor:"#574a3b",
  ratingColor:"#e5c92d",
  viewsColor:"#7f735d",
  lightColor:"#ffffff",
  redBaseColor:"#531901",
  tintColor,
  tabIconDefault: '#ffffff',
  tabIconSelected: "#19502c",
  tabBar: "#1e1e1e",
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
