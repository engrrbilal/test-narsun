import React, { useState } from 'react';
import { Container, Content, Text, Tabs, Tab, StyleProvider } from 'native-base';
import LinksTabsContent from './LinksTabsContent';
import * as apiRoutes from "../../apiroutes"
import axios from 'axios';
import Colors from '../../constants/Colors';
import { StyleSheet } from 'react-native';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';

export default LinksTabs = (props) => {
    const [tabPage, setTabPage] = useState(1);
    const [usersData, setUsersData] = useState([]);
    const [usersIntialData, setUsersIntialData] = useState([]);
    const [isLoading, setisLoading] = useState(false);
    const [error, seterrorr] = useState(false);

    /* mounting re-render for fetching new data when focused */
    React.useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            console.log("#navigation mount")
            fetchData();
        });
        return unsubscribe;
    }, [props]);

    /* Implemented for sorting data when search input changes*/
    React.useEffect(() => {
        console.log("#searching")
        searchForm(props.searchTerm)
    }, [props.searchTerm]);

    const onChangeTab = (changeTabProps) => {
        const newTabIndex = changeTabProps.i;
        setTabPage(newTabIndex);
    };

    function searchForm(term) {
        let searchedData = []
        console.log("Term: ", term)
        term && usersData && usersData.map((user) => {
            if (user && term && (user.name.toLowerCase().includes(term.toLowerCase()) || user.rating.toString().includes(term.toLowerCase()))) {
                searchedData.push(user)
            }
        })
        const tabsData = searchedData.length <= 0 && !term ? usersIntialData : searchedData
        console.log("#tabsData : ",tabsData)
        setUsersData(tabsData)
    }
    function fetchData() {
        setisLoading(true)
        axios
            .get(apiRoutes.dataSource)
            .then(response => {
                // console.log("#response.data : ", response.data.data)
                setisLoading(false)
                setUsersData(response.data.data)
                setUsersIntialData(response.data.data)
            }).catch((error) => {
                console.log("error: ", error)
                setisLoading(false)
                seterrorr(true)
            })
    }
    return (
        <StyleProvider style={getTheme(material)}>

            <Container>
                <Tabs
                    page={tabPage}
                    initialPage={1}
                    tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                    onChangeTab={onChangeTab}
                >
                    <Tab heading="Top"
                        tabStyle={styles.tabStyle}
                        activeTextStyle={styles.activeTabStyle}
                    >
                        <LinksTabsContent usersData={usersData} isLoading={isLoading} error={error} />
                    </Tab>
                    <Tab heading="Peoples"
                        tabStyle={styles.tabStyle}
                        activeTextStyle={styles.activeTabStyle}
                    >
                        <LinksTabsContent usersData={usersData} isLoading={isLoading} error={error} />
                    </Tab>
                    <Tab heading="Tags"
                        tabStyle={styles.tabStyle}
                        activeTextStyle={styles.activeTabStyle}
                    >
                        <LinksTabsContent usersData={usersData} isLoading={isLoading} error={error} />
                    </Tab>
                </Tabs>
            </Container>
        </StyleProvider>
    )
}
const styles = StyleSheet.create({

    tabBarUnderlineStyle: {
        borderBottomColor: Colors.tabsThemeColor,
        backgroundColor: Colors.tabsThemeColor,
        borderBottomWidth: 1.5,
        height: 1
    },
    tabStyle: {
        borderBottomColor: Colors.lightColor,
        borderBottomWidth: 1.5
    },
    activeTabStyle: {
        color: Colors.tabsThemeColor,
        fontWeight: 'bold'
    }
})