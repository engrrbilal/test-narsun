import React, { Component } from 'react';
import {
    Container, Text, Body, Left, Right, Icon, Content, List, ListItem,
    Thumbnail
} from 'native-base';
import { Image, StyleSheet, View, ScrollView, ActivityIndicator } from 'react-native';
import Colors from '../../constants/Colors';

export default class LinksTabsContent extends Component {
    render() {
        return (
            <Container style={styles.container} >
                <Content>
                    <List>
                        {this.props.error && !this.props.usersData ?
                            <View>
                                <Text style={styles.defaultTextStyle}>Something went wrong error while getting data !</Text>
                            </View> : undefined}
                        {this.props.usersData && this.props.usersData.length ?
                            this.props.usersData.map((user, index) => {
                                // console.log("#user : ", user)
                                return (
                                    <ListItem thumbnail key={index}>
                                        <Left>
                                            <Thumbnail source={{ uri: user["profile_image"] }} />
                                        </Left>
                                        <Body style={{ borderBottomColor: Colors.tabsSepratorColor }}>
                                            <Text style={styles.titleStyle}>{user.name}</Text>
                                            <View style={styles.bodyContainer}>
                                                <Text note numberOfLines={1} style={{ color: Colors.tabsSepratorColor, fontSize: 16 }}>{user.rating}</Text>
                                                <Icon name="star" style={styles.bodyRatingStyle} />
                                                <Icon name="star" style={styles.bodyRatingStyle} />
                                                <Icon name="star" style={styles.bodyRatingStyle} />
                                                <Icon name="star" style={styles.bodyRatingStyle} />
                                                <Icon name="star" style={styles.bodyRatingStyle} />
                                                <Text note numberOfLines={1} style={styles.bodyRatingStyle}>{user["review_count"]}(reviews)</Text>
                                            </View>
                                            <View style={styles.bodyContainer}>
                                                {user.tags ? user.tags.map((tag) => {
                                                    return (
                                                        <Text style={styles.tagTextStyle} key={tag}>#{tag}</Text>
                                                    )
                                                }) : undefined}
                                            </View>
                                        </Body>
                                        <Right style={styles.tabsRightContainer}>
                                            <View style={styles.seprator}></View>
                                            <View>
                                                <Text style={styles.currencyStyle}>${user.rate}</Text>
                                            </View>
                                        </Right>
                                    </ListItem>
                                )
                            })
                            :
                            !this.props.isLoading ?
                                <View>
                                    <Text style={styles.defaultTextStyle}>No Item to show</Text>
                                </View> : <ActivityIndicator size="large" color={Colors.tabsThemeColor}/>
                        }
                    </List>
                </Content>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        textAlign: "center",
        backgroundColor: Colors.tabsBackgroundColor
    },
    bodyContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        height: 30
    },
    tagTextStyle: {
        color: Colors.lightColor,
        fontSize: 14,
    },
    titleStyle: {
        color: Colors.lightColor,
        fontSize: 18,
        fontWeight: "600"
    },
    bodyRatingStyle: {
        color: Colors.ratingColor,
         fontSize: 18,
         paddingRight:2
    },
    tabsRightContainer: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        borderBottomColor: Colors.tabsSepratorColor
    },
    seprator: {
        borderRightWidth: 1.5,
        borderColor: Colors.tabsSepratorColor,
        height: 80
    },
    currencyStyle: {
        color: Colors.tabsThemeColor,
        fontSize: 20,
        paddingLeft: 5
    },
    defaultTextStyle: {
        color: Colors.redBaseColor,
        textAlign: "center"
    }
});