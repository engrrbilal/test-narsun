import { Ionicons } from '@expo/vector-icons';
import * as React from 'react';

import Colors from '../constants/Colors';
import { Icon } from 'native-base';

export default function TabBarIcon(props) {
  return (
    <Icon
      name={props.name}
      style={{ marginBottom: -3,
        color:props.focused ? Colors.tabIconSelected : Colors.tabIconDefault,
        fontSize:props.focused?32:24
      }}
      type={props.type ? props.type : "AntDesign"}
    />
  );
}
